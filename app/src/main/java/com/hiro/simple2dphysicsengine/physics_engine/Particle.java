package com.hiro.simple2dphysicsengine.physics_engine;

/**
 * Represents a particle
 */
public class Particle
{
    private Vector position;
    private Vector velocity;

    private double radius;
    private double mass;

    /* Constructors ***************************************************************************************************/

    public Particle(double radius, double mass)
    {
        position = new Vector(0.f, 0.f);
        velocity = new Vector(0.f, 0.f);

        this.radius = radius;
        this.mass = mass;
    }


    /* Getters and setters ********************************************************************************************/

    public double getRadius()
    {
        return radius;
    }
    
    public double getMass()
    {
        return mass;
    }

    public Vector getPosition()
    {
        return position;
    }

    public void setPosition(Vector position)
    {
        this.position = position;
    }

    public Vector getVelocity()
    {
        return velocity;
    }

    public void setVelocity(Vector velocity)
    {
        this.velocity = velocity;
    }

    public void setRadius(double radius)
    {
        this.radius = radius;
    }
}

